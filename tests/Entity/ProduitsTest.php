<?php

namespace App\Tests\Entity;

use App\Entity\Produits;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;

class ProduitsTest extends TestCase
{
    public function testGET()
    {
        $client = HttpClient::create();
        $responses = $client->request('GET', 'http://127.0.0.1:8000/api/produits?page=1');
        $statusCode = $responses->getStatusCode();
        if ($statusCode == 200 || $statusCode == 201) {
            // $content = '{"@context":"\/api\/contexts\/Produits","@id":"\/api\/produits","@type":"hydra:Collection","hydra:member":[{"@id":"\/api\/produits\/1","@type":"Produits","id":1,"name":"Cyberpunk 2077","ref":"C2077","prix":"60"}],"hydra:totalItems":2,"hydra:view":{"@id":"\/api\/produits?page=1","@type":"hydra:PartialCollectionView","hydra:first":"\/api\/produits?page=1","hydra:last":"\/api\/produits?page=2","hydra:next":"\/api\/produits?page=2"}}';
            // $this->assertEquals($content, $responses->getContent());

            $request = $client->request('POST', 'http://127.0.0.1:8000/api/produits', [
                'json' => ['name' => 'Test', 'ref' => 'T01', 'prix' => '50']
            ]);
            $decodedPayload = $request->toArray();

            $this->assertEquals(201, $request->getStatusCode());
        } else {
            print 'error : ' . $statusCode;
        }
    }
}